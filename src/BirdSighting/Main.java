package BirdSighting;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		int[] arr = {2,2,2,2,1,1,1,1,0,2,2,1,0,0,0};
		int n = arr.length;
		int nOfUnsortedArray = 2;
		int[] sortedArray = insertionSort(nOfUnsortedArray, n, arr);
		int[] typeArray = countType(sortedArray);
		System.out.println(findType(typeArray));

	}
	public static int[] insertionSort(int nOfUnsortedArray,int n,int[] arr) {
		for(int i = nOfUnsortedArray-1;i>0;i--) {
			if(arr[i] < arr[i-1]) {
				int temp = arr[i];
				arr[i] = arr[i-1];
				arr[i-1] = temp;
			}
			else {
				break;
			}
		}
		if(nOfUnsortedArray == n) {
			return arr;
		}
		else {
			return insertionSort(nOfUnsortedArray+1, n, arr);
		}
		

	}
	public static int[] countType(int[] arr) {
		int length = arr.length;
		int maxType = arr[length-1];
		int[] typeArr = new int[maxType + 1];
		int marker = 0;
		boolean end = false;
		for(int type = 0; type <= maxType;) {
			int counter = 0;
			for(int i = marker;i<length;i++) {
				if(type == arr[i]) {
					counter++;
					if((i == length -1) && (type == maxType)) {
						typeArr[type] = counter;
						end = true;
						break;
					}
				}
				else {
					marker = i;
					typeArr[type] = counter;
					type++;
					break;
				}
			}
			if ((type == maxType)  && (end == true) ) {
				break;
			}
		}
		return typeArr;
	}
	public static int findType(int[] arr) {
		int length = arr.length;
		int big = arr[0];
		int retValue = 0;
		for(int i = 0;i<length;i++) {
			if(arr[i]>big) {
				big = arr[i];
			}
		}
		for(int i = 0;i<length;i++) {
			if(big == arr[i]) {
				retValue = i;
				break;
			}
		}
		return retValue;
	}

}
